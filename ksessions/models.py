from django.db import models
from django.conf import settings


class SessionActivity(models.Model):
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    session_key = models.CharField(max_length=50)
    date_logout = models.DateTimeField(null=True)
    ip_address = models.GenericIPAddressField(null=True)
    user_agent = models.TextField(null=True)
    is_active = models.BooleanField(default=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.session_key

    class Meta:
        verbose_name_plural = "Session activities"


