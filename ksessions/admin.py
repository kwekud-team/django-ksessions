from django.contrib import admin

from ksessions.models import SessionActivity


@admin.register(SessionActivity)
class SessionActivityAdmin(admin.ModelAdmin):
    readonly_fields = ("user", "ip_address", "date_created", "date_logout", 'session_key', 'site')
    list_display = ('session_key', "user", "ip_address", "user_agent", "date_created", 'date_modified', "date_logout",
                    'site')
    list_select_related = True
    search_fields = ['user__username', "ip_address", "user_agent", "date_created", "date_logout"]

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return request.user.is_superuser

    def has_delete_permission(self, request, obj=None):
        return request.user.is_superuser
