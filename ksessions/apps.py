from django.apps import AppConfig


class CustomConfig(AppConfig):
    name = 'ksessions'

    def ready(self):
        from ksessions.signals import record_login_activity, record_logout_activity
