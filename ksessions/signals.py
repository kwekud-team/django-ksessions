from django.dispatch import receiver

from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.utils import timezone
from django.contrib.sites.shortcuts import get_current_site

from ksessions.models import SessionActivity


@receiver(user_logged_in)
def record_login_activity(request, user, **kwargs):
    """
    Start session activity tracking for newly logged-in user.
    """
    session = request.session

    if user.is_authenticated and session.session_key:
        site = get_current_site(request)
        ip_address = request.META.get("REMOTE_ADDR", None)
        user_agent = request.META.get("HTTP_USER_AGENT", "")

        SessionActivity.objects.update_or_create(
            site=site,
            user=user,
            ip_address=ip_address,
            user_agent=user_agent,
            date_logout__isnull=True,
            defaults={
                'session_key': session.session_key
            }
        )


@receiver(user_logged_out)
def record_logout_activity(request, user, **kwargs):
    """
    Marks end of the session activity.
    Should be called when user logs out or when a session is deactivated.
    """
    session_key = request.session.session_key
    if session_key:
        SessionActivity.objects.filter(session_key=session_key).update(
            date_logout=timezone.now()
        )
