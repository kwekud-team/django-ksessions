from django.urls import path

from ksessions.views import session_list_view, sign_out_other_view


app_name = 'ksessions'


urlpatterns = [
    path("", session_list_view, name="session_activity_list"),
    path("sign-out-other/", sign_out_other_view, name="session_sign_out"),
]
